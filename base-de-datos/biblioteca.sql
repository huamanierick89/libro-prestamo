alter procedure sp_usuario
as
begin
	select U.CodUsuario, U.NombreUsuario from TPrestamo P
	inner join TUsuario U on U.CodUsuario = P.CodUsuario
	inner join TLibro L on L.CodLibro = P.CodLibro
end

exec sp_usuario

alter procedure sp_libro_prestamo
	@codUsuario int
as
begin
	select U.CodUsuario, U.NombreUsuario, U.Telefono, U.Correo, L.Titulo, L.Genero, P.FechaPrestamo, P.FechaDevolucion from TPrestamo P
	inner join TUsuario U on U.CodUsuario = P.CodUsuario
	inner join TLibro L on L.CodLibro = P.CodLibro
	where U.CodUsuario = @codUsuario
end

exec sp_libro_prestamo 100002

