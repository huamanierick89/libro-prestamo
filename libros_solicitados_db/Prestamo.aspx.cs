﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace libros_solicitados_db
{
    public partial class Prestamo : System.Web.UI.Page
    {
        server_connection server= new server_connection();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                dlCodigo.DataSource = server.Query_execution("sp_usuario").Tables[0];
                dlCodigo.DataTextField = "CodUsuario";
                dlCodigo.DataValueField = "NombreUsuario";
                dlCodigo.DataBind();
            } 
        }

        protected void btnCalcular_Click(object sender, EventArgs e)
        {
            Int32 codUsuario = Convert.ToInt32(dlCodigo.SelectedItem.ToString());
            string sql_query = "sp_libro_prestamo " + codUsuario;

            gvLibroSolicitado.DataSource = server.Query_execution(sql_query).Tables[0];
            gvLibroSolicitado.DataBind();
        }
    }
}